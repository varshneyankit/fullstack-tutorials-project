const express = require("express");

const { validateRequestBody } = require("../middlewares/tutorial.middleware");
const {
  createTutorial,
  findAllTutorials,
  findAllPublishedTutorials,
  findTutorial,
  updateTutorialById,
  deleteAllTutorials,
  deleteTutorial,
} = require("../controllers/tutorial.controller");

const router = express.Router();

router.post("/", validateRequestBody, createTutorial);

router.get("/", findAllTutorials);

router.get("/published", findAllPublishedTutorials);

router.get("/:id", findTutorial);

router.put("/:id", validateRequestBody, updateTutorialById);

router.delete("/", deleteAllTutorials);

router.delete("/:id", deleteTutorial);

module.exports = router;
