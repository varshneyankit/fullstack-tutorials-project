const express = require("express");
const dotenv = require("dotenv");
dotenv.config();

const tutorialRoutes = require("./routes/tutorial.routes");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.status(200).json({
    success: true,
    message: "Welcome to my application!!",
    data: {},
    error: {},
  });
});

app.use("/api/tutorials", tutorialRoutes);

app.listen(process.env.PORT, () => {
  console.log(`Server is up and running on PORT ${process.env.PORT}`);
});
