const Tutorial = require("../models/tutorial.model");

function createTutorial(req, res) {
  const tutorial = new Tutorial({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published,
  });

  Tutorial.create(tutorial)
    .then((response) => {
      return res.status(201).json({
        success: true,
        message: "Successfully created tutorial",
        data: response.rows[0],
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: "Something went wrong while creating tutorial!",
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function findAllTutorials(req, res) {
  const title = req.query.title;

  Tutorial.getAll(title)
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: "Successfully fetched all tutorials",
        data: response.rows,
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: "Something went wrong while fetching all tutorials!",
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function findAllPublishedTutorials(req, res) {
  Tutorial.getAllPublished()
    .then((response) => {
      if (response.rowCount === 0) {
        return res.status(404).json({
          success: false,
          message: "No published tutorials found!",
          data: {},
          error: {},
        });
      }

      return res.status(200).json({
        success: true,
        message: "Successfully fetched all published tutorials",
        data: response.rows,
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: "Something went wrong while fetching published tutorials!",
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function findTutorial(req, res) {
  Tutorial.get(req.params.id)
    .then((response) => {
      if (response.rowCount === 0) {
        return res.status(404).json({
          success: false,
          message: `Tutorial with id ${req.params.id} is not found!`,
          data: {},
          error: {},
        });
      }

      return res.status(200).json({
        success: true,
        message: "Successfully fetched a tutorial",
        data: response.rows[0],
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: `Something went wrong while fetching tutorial with id ${req.params.id}!`,
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function updateTutorialById(req, res) {
  Tutorial.update(req.params.id, new Tutorial(req.body))
    .then((response) => {
      if (response.rowCount === 0) {
        return res.status(404).json({
          success: false,
          message: `Tutorial with id ${req.params.id} is not found!`,
          data: {},
          error: {},
        });
      }

      return res.status(200).json({
        success: true,
        message: `Successfully updated tutorial with id ${req.params.id}`,
        data: response.rows[0],
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: `Something went wrong while updating tutorial with id ${req.params.id}!`,
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function deleteAllTutorials(req, res) {
  Tutorial.removeAll()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: "Successfully deleted all tutorials",
        data: {},
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: "Something went wrong while deleting all tutorials!",
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

function deleteTutorial(req, res) {
  Tutorial.remove(req.params.id)
    .then((response) => {
      if (response.rowCount === 0) {
        return res.status(404).json({
          success: false,
          message: `Tutorial with id ${req.params.id} is not found!`,
          data: {},
          error: {},
        });
      }

      return res.status(200).json({
        success: true,
        message: `Successfully deleted tutorial with id ${req.params.id}`,
        data: {},
        error: {},
      });
    })
    .catch((error) => {
      return res.status(500).json({
        success: false,
        message: `Something went wrong while deleting tutorial with id ${req.params.id}!`,
        data: {},
        error: {
          message: error.message,
        },
      });
    });
}

module.exports = {
  createTutorial,
  findAllTutorials,
  findAllPublishedTutorials,
  findTutorial,
  updateTutorialById,
  deleteAllTutorials,
  deleteTutorial,
};
