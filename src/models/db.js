const { Pool } = require("pg");

const dbConfig = require("../config/db.config");

const pool = new Pool({
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  host: dbConfig.HOST,
  port: dbConfig.PORT,
  database: dbConfig.DATABASE,
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};
