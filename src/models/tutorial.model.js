const db = require("./db");

const Tutorial = function (tutorial) {
  this.title = tutorial.title;
  this.description = tutorial.description;
  this.published = tutorial.published;
};

Tutorial.create = ({ title, description, published }) => {
  return new Promise((resolve, reject) => {
    const text =
      "INSERT INTO tutorials(title, description, published) VALUES($1, $2, $3) RETURNING *";
    const values = [title, description, published];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.getAll = (title) => {
  return new Promise((resolve, reject) => {
    let text = "SELECT * FROM tutorials";

    if (title) {
      text += ` WHERE title LIKE '%${title}%'`;
    }

    db.query(text)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.getAllPublished = () => {
  return new Promise((resolve, reject) => {
    const text = "SELECT * FROM tutorials WHERE published=true";
    const values = [];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.get = (id) => {
  return new Promise((resolve, reject) => {
    const text = "SELECT * FROM tutorials WHERE id=$1";
    const values = [id];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.update = (id, tutorial) => {
  return new Promise((resolve, reject) => {
    const text =
      "UPDATE tutorials set title = $1, description = $2, published = $3 WHERE id = $4 RETURNING *";
    const values = [
      tutorial.title,
      tutorial.description,
      tutorial.published,
      id,
    ];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.removeAll = () => {
  return new Promise((resolve, reject) => {
    const text = "DELETE FROM tutorials";
    const values = [];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

Tutorial.remove = (id) => {
  return new Promise((resolve, reject) => {
    const text = "DELETE FROM tutorials WHERE id = $1";
    const values = [id];

    db.query(text, values)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

module.exports = Tutorial;
