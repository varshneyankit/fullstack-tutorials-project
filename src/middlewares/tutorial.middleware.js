function validateRequestBody(req, res, next) {
  if (!req.body.title || !req.body.description || !req.body.published) {
    return res.status(400).json({
      success: false,
      message: "Please pass all required parameters!",
      data: {},
      error: {},
    });
  }
  next();
}

module.exports = {
  validateRequestBody,
};
